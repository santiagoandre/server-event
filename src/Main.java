import fast_k_end.poo_patterns.observer.ObserverCode;
import java.awt.EventQueue;






import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observer;

import model.system.ImpSystem;
import view.AdminAction;
import view.AdminView;
import view.EventView;
import view.EventsController;
import view.LoginSesionController;
import view.UserView;
import view.LoginSesionView;
import view.sesion.AdminSesion;
import view.sesion.UserSesion;


public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					init();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public static void init(){

		ImpSystem system = new ImpSystem();
		AdminSesion sesion = new AdminSesion(system);
		EventView ev = new EventView();
		ev.notify(system, ObserverCode.LOADCATEGORYS);
		AdminView view = new AdminView(ev);
		AdminAction controller = new AdminAction(sesion,view);
		
		sesion.addObserver((fast_k_end.poo_patterns.observer.Observer)  view);
		view.addController(controller);
		ev.setController(controller);
		view.setVisible(true);
	}
}
