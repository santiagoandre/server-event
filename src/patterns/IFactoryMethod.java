package patterns;
import fast_k_end.persistence_system.IPersistence;
import fast_k_end.user.AbsUser;
import fast_k_end.user.Contact;
import java.util.Date;
import model.events.AbsCategory;
import model.events.AbsEvent;
public interface IFactoryMethod {

	public AbsEvent createEvent(String name,Date date,String comment, AbsCategory category);
	public AbsEvent createEvent(String name,String sdate,String comment, AbsCategory category);
		
	public AbsCategory createCategory(String name, String comment);
	public AbsUser createUser(String name,String lastName,String user,String password,String email, int phone);
	public Contact createContact(String email, int phone);
	public AbsUser createUser(String user,String password);

	public IPersistence createSystemPersitence();

	
	
	

}
