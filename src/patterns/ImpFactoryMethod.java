package patterns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import High.ITimer;
import Low.TimerSRT;
import fast_k_end.persistence_system.FilePersistence;
import fast_k_end.persistence_system.IPersistence;
import fast_k_end.user.*;
import model.events.AbsCategory;
import model.events.AbsEvent;
import model.events.SimpleCategory;
import model.events.SimpleEvent;
public class ImpFactoryMethod  implements IFactoryMethod{

	public static SimpleDateFormat datesFormat = new SimpleDateFormat("yyyy-MM-dd=HH:mm");
	public static SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public AbsEvent createEvent(String name,String sdate,String comment, AbsCategory category){
		
		Date date;
		try {
			date = datesFormat.parse(sdate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return createEvent(name,date,comment,category);
		
	}
	@Override
	public AbsEvent createEvent(String name, Date date, String comment,
			AbsCategory category) {
		createTimer();
		return new SimpleEvent(name,comment,date,createTimer(),category);
	}
	public AbsCategory createCategory(String name, String comment){
		return new SimpleCategory(name,comment);
		
	}
	public AbsUser createUser(String name,String lastName,String user,String password,String email, int phone) {
		User u =  new User(name,lastName,user,password);
		u.addContact(createContact(email,phone));
		return u;
		
	}
	public Contact createContact(String email, int phone) {
		return new Contact(email,phone);
	}
	public AbsUser createUser(String user,String password) {
		return new Admin(user,password);		
	}
	public IPersistence createSystemPersitence(){
		return new 	FilePersistence();
	}
	private ITimer createTimer(){
		return TimerSRT.getInstance();
	}
	
	

}
