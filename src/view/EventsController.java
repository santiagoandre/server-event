package view;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import view.sesion.UserSesion;

public class EventsController implements  TableModelListener {
	private UserView view;
	private UserSesion sesion;

	public EventsController(UserView view, UserSesion sesion) {
		
		this.view = view;
		this.sesion = sesion;
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		if (e.getType() == TableModelEvent.UPDATE)
        {
            int row = e.getFirstRow();
            int column = e.getColumn();
            if(column == 5){// se modifico la columna asistir
            	if(view.asisteAlEvento(row))
            		sesion.asistirEvento(view.getNameEvent(row));
            	else
            		sesion.desasistirEvento(view.getNameEvent(row));            		
            }
		
        }
	}

}
