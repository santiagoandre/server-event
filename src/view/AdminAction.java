package view;

import fast_k_end.poo_patterns.observer.ObserverCode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JLabel;

import view.sesion.AdminSesion;
import view.sesion.UserSesion;
import model.system.IAdminSystem;
import model.system.ImpSystem;

public class AdminAction extends  MouseAdapter implements ActionListener{
	private ImpSystem system;
	private AdminSesion sesion;
	private AdminView view;
	 public AdminAction(AdminSesion sesion, AdminView view) {
		 this.sesion = sesion;
		 this.system = (ImpSystem) sesion.getSystem();
		 this.view = view;
	}
	@Override
     public void mouseClicked(MouseEvent e) {
		String action = view.getAction(e.getSource());
		switch(action){
			case "Crear evento   ":
				view.setView(AdminView.CREATE_EVENT);
				break;
			case "Eliminar evento   ":
				view.setView(AdminView.DEL_EVENT);
				break;
			case "Modificar evento  ":
				view.setView(AdminView.MODIFY_EVENT);
				break;
			case "Lanzar cliente   ":
				launchClientView();
				break;			
		}
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = view.getAction(e.getSource());
		switch(action){
			case "CREAR":;
				createEvent(view.getCreateEventView());
		}
		
	}
	 private void createEvent(EventView v) {
		String name = v.getName();
		Date date = v.getDate();
		String comment = v.getComment();	
		String category = v.getCategory();
		
		sesion.crear_evento(name, date, comment, category);
	}
	private void  launchClientView(){
		final UserSesion sesion = new UserSesion(system);
		LoginSesionView viewlogin = new LoginSesionView();
		LoginSesionController controllersesion = new LoginSesionController(sesion,viewlogin);		
		viewlogin.setController(controllersesion);
		final UserView clientView = new UserView(viewlogin);
		EventsController controllerevents = new EventsController(clientView,sesion); 
		clientView.setControllerEvents(controllerevents);
		clientView.setControllerLoginSesion(new MouseAdapter() {
			
            @Override
            public void mouseClicked(MouseEvent e) {
       		 	
                if(!sesion.isLogin()){
                	clientView.setCurrentView(clientView.LOGIN);
                }else{
                	sesion.log_out();
                	System.out.println("se cerro seccion");
                }
            }

        });
		clientView.notify(sesion, ObserverCode.LOG_OUT);
		system.addObserver(sesion);
		sesion.addObserver(clientView);
		clientView.setVisible(true);
	 }

} 
