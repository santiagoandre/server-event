package view;

import fast_k_end.poo_patterns.observer.Observed;
import fast_k_end.poo_patterns.observer.Observer;
import fast_k_end.poo_patterns.observer.ObserverCode;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class AdminView extends JFrame implements Observer {

	public static final int CREATE_EVENT = 0;
	public static final int DEL_EVENT = 1;
	public static final int MODIFY_EVENT = 2;
	private  EventView eventview;
	private ArrayList<JLabel> lblActions;
	
	public AdminView(EventView eventview) {

		//getContentPane().setBackground(new Color(50, 51, 102));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 570, 300);
		getContentPane().setLayout(new BorderLayout(0, 0));
		initPanelActions();
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(50, 51, 102));
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JLabel lblAdminTest = new JLabel("Admin Test   ");
		lblAdminTest.setForeground(new Color(255, 255, 255));
		lblAdminTest.setFont(new Font("Tahoma", Font.ITALIC, 16));
		panel_1.add(lblAdminTest, BorderLayout.EAST);
		this.eventview = eventview;
		add(eventview);
		repaint();
	}


	private void initPanelActions() {
		JPanel panelActions = new JPanel();
		panelActions.setBackground(new Color(50, 51, 102));
		getContentPane().add(panelActions, BorderLayout.NORTH);
		panelActions.setLayout(new BoxLayout(panelActions,BoxLayout.X_AXIS));
		
		JLabel lblTopevents = new JLabel("TopEvents      ");		
		lblTopevents.setForeground(new Color(255, 255, 255));
		lblTopevents.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		panelActions.add(lblTopevents);
		initActionLabels();
		for(JLabel action: lblActions){
			action.setForeground(new Color(245, 220, 245));
			action.setFont(new Font("Segoe UI", Font.PLAIN, 15));
			panelActions.add(action);
		}		
	}


	private void initActionLabels() {
		lblActions = new ArrayList<>();
		lblActions.add(new JLabel("Crear evento   "));
		lblActions.add( new JLabel("Eliminar evento   "));
		lblActions.add(new JLabel("Modificar evento  "));
		lblActions.add( new JLabel("Lanzar cliente   "));
		
	}


	public void addController(MouseAdapter controller) {
		for(JLabel action: lblActions){
			action.addMouseListener(controller);
		}
			
		
	}


	public String getAction(Object source) {
		if(source.getClass().equals(JLabel.class)){
		JLabel label = (JLabel) source;
		return label.getText();
		}
		JButton button = (JButton) source;
		return button.getText();
	}


	public EventView getCreateEventView() {
		return eventview;
	}


	public void setView(int idview){
		switch(idview){
		case CREATE_EVENT:
			eventview.notify(null, ObserverCode.CREATEEVENT);
			break;
			
		case DEL_EVENT:
			eventview.notify(null, ObserverCode.DEL_EVENT);
			break;
			
		case MODIFY_EVENT:

			eventview.notify(null, ObserverCode.MODIFY_EVENT);
			break;
			
		}

		this.revalidate();this.repaint();
		
		
	}


	@Override
	public void notify(Observed observed, ObserverCode code) {
		System.out.println("Code: " + code);
		
	}

}
