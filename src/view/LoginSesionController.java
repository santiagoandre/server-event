package view;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import view.sesion.ISesion;

public class LoginSesionController implements ActionListener{
	private ISesion sesion;
	private LoginSesionView view;
	
	public LoginSesionController(ISesion sesion, LoginSesionView view) {
		this.sesion = sesion;
		this.view = view;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String username = view.getUser();
		String password = view.getPasword();
		sesion.log_in(username, password);
		
	}
	
}
