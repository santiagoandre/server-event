package view;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

import com.toedter.calendar.JDateChooser;
import fast_k_end.poo_patterns.observer.*;

import model.events.AbsCategory;
import model.events.AbsEvent;
import model.system.IUserSystem;
import patterns.ImpFactoryMethod;


public class EventView extends JPanel implements Observer {
	private JTextField tbxName;
	private JDateChooser jdDate;
	private JFormattedTextField jdHour;
	private JTextArea tbxComment;
	private JComboBox jcbCategorys;
	private JButton btnCrear;
	/**
	 * Create the panel.
	 */
	public EventView() {
		setLayout(null);
		
		JLabel lblCrearEvento = new JLabel("Crear Evento");
		lblCrearEvento.setBounds(196, 11, 122, 30);
		lblCrearEvento.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		add(lblCrearEvento);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(54, 52, 63, 14);
		lblNombre.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		add(lblNombre);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setBounds(65, 146, 46, 14);
		lblFecha.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		add(lblFecha);
		
		JLabel lblComentario = new JLabel("Comentario:");
		lblComentario.setBounds(277, 52, 82, 14);
		lblComentario.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		add(lblComentario);
		
		JLabel lblCategoria = new JLabel("Categoria:");
		lblCategoria.setBounds(44, 98, 63, 14);
		lblCategoria.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		add(lblCategoria);
		
		tbxComment= new JTextArea();
		tbxComment.setBounds(369, 49, 104, 63);
		add(tbxComment);
		
		tbxName = new JTextField();
		tbxName.setBounds(116, 52, 104, 17);
		add(tbxName);
		tbxName.setColumns(10);
		
		jdDate = new JDateChooser();
		jdDate.setBounds(116, 146, 104, 17);
		add(jdDate);
		
		
		jdHour = new  JFormattedTextField(ImpFactoryMethod.hourFormat);
		jdHour.setBounds(367, 144, 86, 20);
		add(jdHour);
		
		btnCrear = new JButton("CREAR");
		btnCrear.setBounds(214, 188, 91, 23);
		btnCrear.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		add(btnCrear);
		
		
		jcbCategorys = new JComboBox();
		jcbCategorys.setBounds(116, 98, 104, 17);
		add(jcbCategorys);
		
		JLabel lblHora = new JLabel("Hora: ");
		lblHora.setBounds(313, 147, 46, 14);
		add(lblHora);
		

		
	}
	public String getName() {
		return this.tbxName.getText();
	}
	public Date getDate() {
		Date  d  = this.jdDate.getDate();
		Date d2;
		try {
			d2 = ImpFactoryMethod.hourFormat.parse(this.jdHour.getText());
		} catch (ParseException e) {
			System.out.println("Formato de hora uinvalida");
			this.jdHour.setText("HH:MM");
			return d;
		}
		d.setHours(d2.getHours());
		d.setMinutes(d2.getMinutes());
		return d;
	}

	public String getComment() {
		return this.tbxComment.getText();
	}

	public String getCategory(){
		return this.jcbCategorys.getSelectedItem().toString();
	}
	@Override
	public void notify(Observed observed, ObserverCode code) {
		if(code == ObserverCode.LOADCATEGORYS){
			loadCategorys((IUserSystem) observed);
			//clearView();
			//this.btnCrear.setText("CREAR");
		}
		else if(code == ObserverCode.MODIFY_EVENT){
			//loadEvent((AbsEvent) observed);

			this.btnCrear.setText("ACTUALIZAR");
		}else if(code == ObserverCode.CREATEEVENT){
			clearView();
			this.btnCrear.setText("CREAR");
		}else if(code == ObserverCode.DEL_EVENT)
			this.btnCrear.setText("ELIMINAR");
	}
	private void clearView(){
		this.tbxName.setText("");
		this.jdDate.setDate(new Date());
		jcbCategorys.setSelectedIndex(0);
		tbxComment.setText("");
	}
	private void loadEvent(AbsEvent event) {
		System.out.println(event);
		this.tbxName.setText(event.getName());
		this.jdDate.setDate(event.getDate());
		String category = event.getCategory().getName();
		int  items = jcbCategorys.getItemCount();
		for(int i = 0; i <items;i++){
			if(jcbCategorys.getItemAt(i).equals(category))
				jcbCategorys.setSelectedIndex(i);
		}

		this.tbxComment.setText(event.getComment());
	}

	private void loadCategorys(IUserSystem system){
		 Vector items=new Vector();
		ArrayList<AbsCategory> categorys = system.getCategorys();
		Object[] metodos =  system.getCategorys().toArray();
		DefaultComboBoxModel  cbxModel = new DefaultComboBoxModel (metodos);
		this.jcbCategorys.setModel(cbxModel);
		
	}

	public void setController(ActionListener controller){
		this.btnCrear.addActionListener(controller);
	}
}
