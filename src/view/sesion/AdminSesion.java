package view.sesion;

import fast_k_end.poo_patterns.observer.*;
import fast_k_end.user.Admin;
import java.util.Date;

import model.system.IAdminSystem;

public class AdminSesion  extends Observed implements ISesion,Observer{
	private IAdminSystem system;
	private Admin user;
	private Admin userLogOut;
	private boolean isLogin;
	public AdminSesion(IAdminSystem system) {
		this.system = system;
		userLogOut = new Admin(null, null);
		user = userLogOut;
		userLogOut.addObserver(this);
		isLogin = false;
		
	}
	public Admin getUser() {
		return user;
	}
	public void setUser(Admin user) {
		this.user = user;
	}
	public IAdminSystem getSystem() {
		return system;
	}
	public void setSystem(IAdminSystem system) {
		this.system = system;
	}

	@Override
	public void log_in(String user, String password) {
		this.user = (Admin) this.system.log_user(user,password);
		if(this.user != null){
			isLogin = true;
			this.user.addObserver(this);
			this.notify_observers(ObserverCode.LOG_IN);
		}else{
			this.notify_observers(ObserverCode.INVALID_CREDENTIALS);
			this.user = this.userLogOut;
		}
		
	}

	
	@Override
	public void log_out() {
		this.user = userLogOut;
		isLogin = false;
		this.notify_observers(ObserverCode.LOG_OUT);
		
	}
	@Override
	public boolean isLogin() {
		return isLogin;
	}
	public void crear_evento(String name, Date date,String comment,String category){
		ObserverCode code = system.create_event(name, date,comment, category);
		this.notify_observers(code);
	}
	public void modificar_evento(String name, String field, String newVaule){
		system.modify_event(name,field,newVaule);
	}
	@Override
	public void notify(Observed observed,ObserverCode code) {
		this.notify_observers(observed,code);//enviarle el evento a las 
									     	//vistas para que estas muestren
									    	//el mensaje de eventos
		
		
	}
	

}
