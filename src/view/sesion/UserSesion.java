package view.sesion;


import fast_k_end.poo_patterns.observer.*;
import fast_k_end.user.User;
import model.events.AbsEvent;
import model.system.IUserSystem;


public class UserSesion  extends Observed  implements ISesion,Observer{
	private User user;
	private User userLogOut;
	private boolean isLogin;
	private IUserSystem system;
	public UserSesion(IUserSystem system) {
		this.system= system;
		userLogOut = new User(null, null, null, null);
		user = userLogOut;
		userLogOut.addObserver(this);
		isLogin = false;
		
	}
	public IUserSystem getSystem(){
		return system;
	}
	public User getUser() {
		return user;
	}
	@Override
	public void log_in(String user, String password) {
		this.user = (User) this.system.log_user(user,password);
		if(this.user != null){
			isLogin = true;
			this.user.addObserver(this);
			transferAsistsInSesion();
			this.notify_observers(ObserverCode.LOG_IN);
		}else
			this.notify_observers(ObserverCode.INVALID_CREDENTIALS);
		
	}

	private void transferAsistsInSesion() {
		for(AbsEvent e: system.getEvents()){
			if(e.isObserver(userLogOut)){
				e.delObserver(userLogOut);
				e.addObserver(user);
			}
		}
		
	}
	@Override
	public void log_out() {
		this.user = userLogOut;
		isLogin = false;
		this.notify_observers(ObserverCode.LOG_OUT);
		
	}
	@Override
	public boolean isLogin() {
		return isLogin;
	}
	
	public void asistirEvento(String name){
		AbsEvent e = system.getEvent(name);
		e.addObserver(user);
		if(e.isObserver(user))
			System.out.println("el usuario " + user.getName()+ " Asistira al evento " + e.getName());
	}
	public void desasistirEvento(String name){
		AbsEvent e = system.getEvent(name);
		e.delObserver(user);

		if(!e.isObserver(user))
			System.out.println("el usuario " + user.getName()+ " no asistira al evento. " + e.getName());
	}
	@Override
	public void notify(Observed observed,ObserverCode code) {
		if(code == ObserverCode.UPDATE)
			observed = this;
		this.notify_observers(observed,code);
			//En la vida real la sesion y la vista son uno uno
			// asi que aqui se le notificara a la unica vista que hay
			
		
		
	}
	
	

}
