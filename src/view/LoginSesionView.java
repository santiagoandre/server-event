package view;



import javax.swing.JPanel;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class LoginSesionView extends JPanel {

	
	private JTextField txtUser;
	private JTextField txtPassword;
	private JLabel lblIniciarSeccion;
	private JButton btnIniciarSeccion;

	public LoginSesionView() {
		
		setBounds(100, 100, 255, 151);
		setLayout(null);
		
		JLabel lblNombre = new JLabel("Usuario: ");
		lblNombre.setBounds(34, 49, 71, 14);
		add(lblNombre);
		
		JLabel lblCotrasena = new JLabel("Cotrasena: ");
		lblCotrasena.setBounds(34, 74, 71, 14);
		add(lblCotrasena);
		
		txtUser = new JTextField();
		txtUser.setBounds(115, 46, 86, 20);
		add(txtUser);
		txtUser.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setBounds(115, 73, 86, 20);
		add(txtPassword);
		txtPassword.setColumns(10);
		
		lblIniciarSeccion = new JLabel("Iniciar Seccion");
		lblIniciarSeccion.setBounds(79, 11, 104, 14);
		add(lblIniciarSeccion);
		
		btnIniciarSeccion = new JButton("Iniciar Seccion");
		btnIniciarSeccion.setBounds(60, 106, 103, 23);
		add(btnIniciarSeccion);
	}
	public void setController(ActionListener a){
		this.btnIniciarSeccion.addActionListener(a);
	}

	public String getUser() {
		return this.txtUser.getText().toString();
		
	}

	public String getPasword() {
		return this.txtPassword.getText().toString();
	}
}
