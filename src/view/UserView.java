package view;

import fast_k_end.poo_patterns.observer.*;
import fast_k_end.user.User;
import java.awt.Color;




import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.events.AbsEvent;
import model.system.IUserSystem;
import patterns.ImpFactoryMethod;
import view.sesion.UserSesion;

import javax.swing.JPanel;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;

public class UserView extends JFrame implements Observer {


	private LoginSesionView loginview;
	private JScrollPane tableContainer;
	private JTable eventsTable;
	private JLabel lblIniciarSeccion;
	public static int EVENTS = 0;
	public static int LOGIN = 1;
	
	public UserView(LoginSesionView view) {
		getContentPane().setBackground(new Color(50, 51, 102));

		//setDefaultCloseOperation(JFrame.);
		//setBounds(100, 100, 459, 300);
		setLoginView(view);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(50, 51, 102));
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JLabel lblTopevents = new JLabel("TopEvents");
		panel.add(lblTopevents);
		lblTopevents.setForeground(new Color(255, 255, 255));
		lblTopevents.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		lblIniciarSeccion = new JLabel("Iniciar Seccion");
		lblIniciarSeccion.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
		lblIniciarSeccion.setForeground(new Color(255, 255, 255));
		lblIniciarSeccion.setFont(new Font("Tahoma", Font.BOLD, 16));
		panel.add(lblIniciarSeccion, BorderLayout.EAST);
		
		tableContainer = new JScrollPane();
		initEventsTable();
		tableContainer.setViewportView(eventsTable);
		//scrollPane.setBounds(0, 0, 407, 150);
		this.setCurrentView(EVENTS);
		
		repaint();
	}
	//------------- TABLE METHODS    --------------------//
	private void initEventsTable() {
		eventsTable =  new JTable(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Nombre", "Categoria", "Fecha", "Hora", "Descripcion", "Asistir"
				}
			) {
				Class[] columnTypes = new Class[] {
					Object.class, Object.class, Object.class, Object.class, Object.class, Boolean.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false, false, false, true
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
	}
	public String getNameEvent(int i) {
		return (String) eventsTable.getModel().getValueAt(i, 0);
	}

	public boolean asisteAlEvento(int i) {

		return (boolean) eventsTable.getModel().getValueAt(i,5);
	}
	//------------- UPDATE VIEW METHODS --------------- //
	public void setCurrentView(int viewID){
		if(viewID == EVENTS){
			this.remove(loginview);
			tableContainer.setBounds(0,200, 500,200);
			this.add(tableContainer,BorderLayout.CENTER);

			this.setBounds(500,260);
			
		}else{
			this.remove(tableContainer);
			this.add(loginview,BorderLayout.CENTER);
			this.lblIniciarSeccion.setVisible(false);
			this.setBounds(255, 200);
		}
		this.validate();
		this.repaint();
	}

	private void setBounds(int width,int height){
		Rectangle r = this.getBounds();
		r.width = width;
		r.height = height;
		this.setBounds(r);
	}
	// ------------ GETERS AN SETERS ----------------// 
	public void setLoginView(LoginSesionView viewlogin){
		loginview = viewlogin;
	}
	public void setControllerEvents(TableModelListener controller){

	    eventsTable.getModel().addTableModelListener(controller);
	    
	}
	public void setControllerLoginSesion(MouseAdapter controller){

	    this.lblIniciarSeccion.addMouseListener(controller);
	    
	}
	//---------------NOTIFY OBVERVED METHODS ----------------//
	@Override
	public void notify(Observed observed,ObserverCode code) {
		if(observed.getClass().equals(UserSesion.class)){
			if(code == ObserverCode.LOG_OUT){
				loadEvents((UserSesion) observed);
				this.lblIniciarSeccion.setText("Iniciar seccion");
			}else{
				notifyLoginSesion(observed,code);
			}
		}
		else if(code == ObserverCode.NOTIFY_EVENT)
			notifyEvent(observed);
		else if(code  == ObserverCode.UPDATE)
			loadEvents((UserSesion) observed);
		repaint();
	}
	private void loadEvents(UserSesion sesion) {
		IUserSystem system = sesion.getSystem();
		User user = sesion.getUser();
		DefaultTableModel model = (DefaultTableModel) eventsTable.getModel();
		model.getDataVector().removeAllElements();
		for(AbsEvent e : system.getEvents()){
			model.addRow(new Object[] { e.getName()
									  , e.getCategory().getName()
									  , ImpFactoryMethod.dateFormat.format(e.getDate())
									  , ImpFactoryMethod.hourFormat.format(e.getDate())
									  , e.getComment()
									  , e.isObserver(user)});
		}
		eventsTable.setModel(model);
		
	}
	private void notifyEvent(Observed observed) {
		AbsEvent e = (AbsEvent) observed;
		String msg = e.getMsgNotificacion();
		JOptionPane.showMessageDialog(null, msg);
	}
	private void notifyLoginSesion(Observed observed, ObserverCode code) {
		UserSesion sesion = (UserSesion) observed;
		if(code == ObserverCode.INVALID_CREDENTIALS)
			JOptionPane.showMessageDialog(null, "Credenciales invalidas");
		else{
			loadEvents(sesion);
			setCurrentView(EVENTS);
			this.lblIniciarSeccion.setText("Cerrar seccion");
			this.lblIniciarSeccion.setVisible(true);
			
		}
	}
}
