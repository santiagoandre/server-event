package model.events;

public class AbsCategory {
	private String name;
	private String comment;
	/*una categoria puede tener mas cosas
	 *asi que dejo esta clase como abstrata
	 */
	public AbsCategory(String name, String comment) {
		super();
		this.name = name;
		this.comment = comment;
	}
	public AbsCategory(String name) {
		super();
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		//return " [name=" + name + ", comment=" + comment + "]";
		return name;
	}
	
}
