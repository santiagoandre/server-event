package model.events;
import java.util.Calendar;
import java.util.Date;

import patterns.ImpFactoryMethod;
import High.ITimer;
import High.TimerClient;
import fast_k_end.poo_patterns.observer.Observed;
import fast_k_end.poo_patterns.observer.ObserverCode;

public class AbsEvent extends Observed implements TimerClient{
	public static int  NOTIFY_EVENT = 0;
	private String name;
	private String comment;
	private Date date;
	private AbsCategory category;
	/*un evento puede tener mas cosas
	 *asi que dejo esta clase como abstrata
	 */
	
	public AbsEvent(String name, Date date,ITimer timer, AbsCategory category){
		this.name= name;
		this.date=date;
		this.category=category;
		schedule(timer);
		
	}
	public AbsEvent(String name,String comment, Date date,ITimer timer,AbsCategory category){
		this(name,date,timer,category);
		this.comment = comment;
	}
	public AbsCategory getCategory() {
		return category;
	}
	public void setCategory(AbsCategory category) {
		this.category = category;
	}
	
	public String getName(){
		return name;
	}
	public Date getDate(){
		return date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	private void schedule(ITimer timer) {
		Calendar c = Calendar.getInstance();
		//notify one week before
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, -7);
		//System.out.println("time week before: "+(System.currentTimeMillis()-c.getTime().getTime()));
		
		timer.schedule(c.getTime(),this);	

		//notify one day before
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, -1);
		timer.schedule(c.getTime(),this);	
		//System.out.println("time day before: "+(System.currentTimeMillis()-c.getTime().getTime()));
		//notify one hour before
		c.setTime(date);
		c.add(Calendar.HOUR, -1);
		//System.out.println("time hour before: "+(System.currentTimeMillis()-c.getTime().getTime()));
		timer.schedule(c.getTime(),this);	
	}

	
	//protected abstract void createTimer();

	@Override
	public void timeOut() {
		super.notify_observers(ObserverCode.NOTIFY_EVENT);
		
	}
	@Override
	public String toString() {
		return "[name=" + name + ", comment=" + comment + ", date="
				+ date + ", category=" + category + "]";
	}
	public String getMsgNotificacion() {
		return "tienes el evento " + getName() 
				+" " + ImpFactoryMethod.dateFormat.format(getDate())
				+" a las" + ImpFactoryMethod.hourFormat.format(getDate());
	}
	

}


