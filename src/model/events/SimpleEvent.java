package model.events;

import java.util.Date;

import High.ITimer;

public class SimpleEvent extends AbsEvent{

	public SimpleEvent(String name, Date date, ITimer timer,
			AbsCategory category) {
		super(name, date, timer, category);
		
	}
	public SimpleEvent(String name, String comment, Date date, ITimer timer,
			AbsCategory category) {
		super(name, comment, date, timer, category);
	}

}
