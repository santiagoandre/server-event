package model.system;

import fast_k_end.poo_patterns.observer.ObserverCode;
import fast_k_end.user.AbsUser;
import java.util.ArrayList;


import java.util.Date;

import model.events.AbsCategory;
import model.events.AbsEvent;
import patterns.ImpFactoryMethod;


public class ImpSystem extends AbsSystem  implements IUserSystem,IAdminSystem{
	@Override
	public AbsUser log_user(String user, String password) {
		String [] crendentials = {user,password};
		String data[] = persistence.getDataOfTarget("user",crendentials);
		if(data != null){
			return createUser(data);
		}
		return null;
	}
	
	@Override
	protected void loadFactory() {
		factory = new ImpFactoryMethod();
		
	}

	@Override
	public ObserverCode create_event(String name, Date date,String comment, String category) {
		if(getEvent(name) == null){
			this.events.add(factory.createEvent(name, date, comment, getCategory(category)));
			this.notify_observers(ObserverCode.UPDATE);
			return ObserverCode.CREATESUCCESS;
		}else
			return ObserverCode.ALREADY_EXIST;
	}
	@Override
	public void modify_event(String name, String field, String newVaule) {
		//AbsEvent e = getEvent(name);
		//if(e == null)
			//	TODO 
		
	}
	@Override
	public AbsEvent getEvent(String name) {
		for(AbsEvent e : events){
			if(e.getName().equals(name))
				return e;
		}
		return null;
	}

	@Override
	public AbsCategory getCategory(String name) {
		for(AbsCategory c : categorys){
			if(c.getName().equals(name))
				return c;
		}
		return null;
	}

	@Override
	public ArrayList<AbsEvent> getEvents() {
		return events;
	}

	@Override
	public ArrayList<AbsCategory> getCategorys() {
		return categorys;
	}


	
}
