package model.system;

import fast_k_end.persistence_system.IPersistence;
import fast_k_end.poo_patterns.observer.Observed;
import fast_k_end.user.AbsUser;
import java.util.ArrayList;

import java.util.List;

import model.events.AbsCategory;
import model.events.AbsEvent;
import patterns.IFactoryMethod;

public abstract class AbsSystem extends Observed {
	protected IPersistence persistence;
	protected IFactoryMethod factory;
	protected ArrayList<AbsEvent> events;
	protected ArrayList<AbsCategory> categorys;
	
	public AbsSystem(){
		loadFactory();
		persistence = factory.createSystemPersitence();
		loadCategorys();
		loadEvents();
		
	}
	
	protected abstract  void loadFactory();
	//------------- methods that speak with paersistence system -------------// 
	private void loadEvents(){
		List<String[]> data = persistence.getData("event");
		ArrayList<AbsEvent> events = new ArrayList<>();
		for(String[] dataEvent: data){

			events.add(createEvent(dataEvent));
		}
		this.events = events;
	}
	private void loadCategorys() {
		List<String[]> data = persistence.getData("category");
		ArrayList<AbsCategory> categorys = new ArrayList<>();
		for(String[] dataEvent: data){
			categorys.add(createCategory(dataEvent));
		}
		this.categorys=categorys;
	}
	//---------------simple geter  methods ----------------//	
	
	
	//------------------ methods that speak with factory class --------------//
	private AbsEvent createEvent(String[] dataEvent) {
		//name, Date date,ITimer timer, AbsCategory category
		for(AbsCategory  c : categorys){
			if(c.getName().equals(dataEvent[3]))
				return createEvent(dataEvent,c);
		}
		return null;
	}
	private AbsEvent createEvent(String[] dataEvent,AbsCategory category) {
		//name, Date date,ITimer timer, AbsCategory category
			return factory.createEvent(dataEvent[0], dataEvent[1], dataEvent[2], category);
	}
	protected AbsUser createUser(String[] data) {
		if(data.length == 2)
			return factory.createUser(data[0],data[1]);
		if(data.length == 6)
			//							(name, last name, user, password, email, phone)
			return factory.createUser(data[2],data[3],data[0],data[1],data[4],Integer.parseInt(data[5]));
		return null;
	}
	private AbsCategory createCategory(String[] data) {
		return factory.createCategory(data[0], data[1]);
	}
	

	

}
