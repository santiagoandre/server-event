package model.system;

import fast_k_end.user.AbsUser;
import java.util.ArrayList;

import model.events.AbsCategory;
import model.events.AbsEvent;

public interface IUserSystem {
	public AbsEvent getEvent(String name);

	public AbsCategory getCategory(String name);

	public ArrayList<AbsEvent> getEvents();
	public AbsUser log_user(String user, String password) ;

	public ArrayList<AbsCategory> getCategorys();
}
