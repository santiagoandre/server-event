package model.system;
import fast_k_end.poo_patterns.observer.ObserverCode;
import fast_k_end.user.AbsUser;
import java.util.Date;


public interface IAdminSystem {
	ObserverCode create_event(String name, Date date, String comment, String category);
	public AbsUser log_user(String user, String password) ;
	public void modify_event(String name, String field, String newVaule);
	
}
